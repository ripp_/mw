[1;4mBitwise Instructions[0m

[1mNAME[0m
     [1mAND[0m - Bitwise AND the two values
     [1mOR[0m  - Bitwise OR the two values
     [1mXOR[0m - Bitwise XOR the two values

[1mSYNOPSIS[0m
     [1mAND[0m [4mvariable[0m [4mvalue[0m
     [1mOR[0m  [4mvariable[0m [4mvalue[0m
     [1mXOR[0m [4mvariable[0m [4mvalue[0m

[1mDESCRIPTION[0m
     These functions act upon the contents of a named [4mvariable[0m and
     a given [4mvalue[0m, the result is then placed in the named [4mvariable[0m.

     Each bit of the [4mvariable[0m's value is logically AND/OR/XORed with
     that of the supplied [4mvalue[0m.

     Both the contents of the [4mvariable[0m, and the [4mvalue[0m must be
     integers, otherwise no action is performed.
     
     The NOT operator can be simulated by using an XOR with a value
     that has every bit set.

Examples
operation     AND          OR           XOR
variable      11001010     11001010     11001010  (202)
value         11110000     11110000     11110000  (240)
result        11000000     11111010     00111010
as integer    192          250          58


# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Justin Mitchell <arthur@sucs.org>
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: mw-devel@lists.sucs.org\n"
"POT-Creation-Date: 2012-11-19 19:21+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../src/add.c:128
#, c-format
msgid "Not allowed to write to this folder.\n"
msgstr ""

#: ../src/add.c:137
#, c-format
msgid "Writing in folder %s.\n"
msgstr ""

#: ../src/add.c:139
#, c-format
msgid "Replying to message %d in folder %s\n"
msgstr ""

#: ../src/add.c:144
#, c-format
msgid "Note: Operators have the right to read any 'private' messages.\n"
msgstr ""

#: ../src/add.c:145
#, c-format
msgid "    : If you don't like this, don't write private messages.\n"
msgstr ""

#: ../src/add.c:163
#, c-format
msgid "There is no message %d\n"
msgstr ""

#: ../src/add.c:173
#, c-format
msgid "reply: Error, could not find old message !\n"
msgstr ""

#: ../src/add.c:189
#, c-format
msgid "Message from %s\n"
msgstr ""

#: ../src/add.c:194 ../src/add.c:216
#, c-format
msgid "Send to: "
msgstr ""

#: ../src/add.c:200 ../src/add.c:220
#, c-format
msgid "Message must be addressed to an existing user.\n"
msgstr ""

#: ../src/add.c:205 ../src/add.c:212 ../src/edit.c:915
#, c-format
msgid "Subject: "
msgstr ""

#: ../src/add.c:210
#, c-format
msgid ""
"\n"
"Enter subject or press <ENTER> to accept old.\n"
msgstr ""

#: ../src/add.c:211
#, c-format
msgid "Subject: %s\n"
msgstr ""

#: ../src/add.c:240
#, c-format
msgid "Enter message ending with a . on a new line\n"
msgstr ""

#: ../src/add.c:272
#, c-format
msgid "Post message, edit, reformat, or Abandon ?"
msgstr ""

#: ../src/add.c:275
msgid "abandon"
msgstr ""

#: ../src/add.c:282
msgid "reformat"
msgstr ""

#: ../src/add.c:284
#, c-format
msgid "Reformatting..."
msgstr ""

#: ../src/add.c:287
#, c-format
msgid "Done.\r\n"
msgstr ""

#: ../src/add.c:289
msgid "edit"
msgstr ""

#: ../src/add.c:308 ../src/edit.c:831
#, c-format
msgid "Failed to set user id, aborting.\n"
msgstr ""

#: ../src/add.c:328
#, c-format
msgid "Cannot create temporary file: %s\n"
msgstr ""

#: ../src/add.c:351 ../src/edit.c:864
#, c-format
msgid "ERROR: Could not spawn editor: %s\n"
msgstr ""

#: ../src/add.c:359
#, c-format
msgid "ERROR: child wait (%d) %s\n"
msgstr ""

#: ../src/add.c:381
msgid "post"
msgstr ""

#: ../src/add.c:423
#, c-format
msgid "Warning: Index error, skipping back %ld bytes.\r\n"
msgstr ""

#: ../src/add.c:446
#, c-format
msgid "Warning: incomplete folder record written; folder file may be corrupt.\n"
msgstr ""

#: ../src/alias.c:127 ../src/alias.c:137 ../src/alias.c:151 ../src/main.c:1006
#: ../src/user.c:414 ../src/user.c:451 ../src/user.c:517
#, c-format
msgid "---more---\r"
msgstr ""

#: ../src/edit.c:147
#, c-format
msgid "Username %s not found.\n"
msgstr ""

#: ../src/edit.c:161
#, c-format
msgid "Current status set to [%s]\n"
msgstr ""

#: ../src/edit.c:164
#, c-format
msgid "New status [+-=][%s]: "
msgstr ""

#: ../src/edit.c:173
#, c-format
msgid "Do you really want to delete this user ? "
msgstr ""

#: ../src/edit.c:182
#, c-format
msgid "Status set to [%s].\n"
msgstr ""

#: ../src/edit.c:196
#, c-format
msgid "Current specials set to [%s]\n"
msgstr ""

#: ../src/edit.c:199
#, c-format
msgid "New specials [+-=][%s]: "
msgstr ""

#: ../src/edit.c:205
#, c-format
msgid "Specials set to [%s].\n"
msgstr ""

#: ../src/edit.c:218
#, c-format
msgid "Current chatprivs set to [%s]\n"
msgstr ""

#: ../src/edit.c:219
#, c-format
msgid "New chatprivs [+-=][%s]: "
msgstr ""

#: ../src/edit.c:224
#, c-format
msgid "Chatprivs set to [%s].\n"
msgstr ""

#: ../src/edit.c:242
#, c-format
msgid "Current protection set to %s\n"
msgstr ""

#: ../src/edit.c:243
#, c-format
msgid "New levels [0-4]/[0-4]: "
msgstr ""

#: ../src/edit.c:254
#, c-format
msgid "Protection set to %d/%d.\n"
msgstr ""

#: ../src/edit.c:264
#, c-format
msgid "Invalid protection level.\n"
msgstr ""

#: ../src/edit.c:274
#, c-format
msgid "Current chatmode set to [%s]\n"
msgstr ""

#: ../src/edit.c:275
#, c-format
msgid "New chatmode [+-=][%s]: "
msgstr ""

#: ../src/edit.c:280
#, c-format
msgid "Chatmode set to [%s].\n"
msgstr ""

#: ../src/edit.c:297
#, c-format
msgid "User currently in groups [%s]\n"
msgstr ""

#: ../src/edit.c:298
#, c-format
msgid "New Groups [+-=][12345678]: "
msgstr ""

#: ../src/edit.c:308 ../src/edit.c:674
#, c-format
msgid "Groups changed to [%s]\n"
msgstr ""

#: ../src/edit.c:317
msgid "New Passwd: "
msgstr ""

#: ../src/edit.c:318 ../src/newmain.c:860
msgid "Again: "
msgstr ""

#: ../src/edit.c:321
#, c-format
msgid ""
"Passwords did not match.\n"
"Not done.\n"
msgstr ""

#: ../src/edit.c:326
#, c-format
msgid "Password changed.\n"
msgstr ""

#: ../src/edit.c:338
#, c-format
msgid "Real Name: %s\n"
msgstr ""

#: ../src/edit.c:339
#, c-format
msgid "Enter new name (%d chars): "
msgstr ""

#: ../src/edit.c:345 ../src/edit.c:420
#, c-format
msgid "New name set.\n"
msgstr ""

#: ../src/edit.c:359
#, c-format
msgid "User currently in room [%d]\n"
msgstr ""

#: ../src/edit.c:360
#, c-format
msgid "New Room: [0-65535]: "
msgstr ""

#: ../src/edit.c:366 ../src/edit.c:372
#, c-format
msgid "Invalid Room ID (0-65535 only)\n"
msgstr ""

#: ../src/edit.c:378
#, c-format
msgid "Room changed to %d.\n"
msgstr ""

#: ../src/edit.c:388
#, c-format
msgid "WARNING: This command can be very dangerous !\n"
msgstr ""

#: ../src/edit.c:389
#, c-format
msgid "User Name: %s\n"
msgstr ""

#: ../src/edit.c:390
#, c-format
msgid "Enter new username (%d chars): "
msgstr ""

#: ../src/edit.c:407
#, c-format
msgid "You cannot change a username to one that already exists.\n"
msgstr ""

#: ../src/edit.c:410
#, c-format
msgid "Attempting to update existing user.\n"
msgstr ""

#: ../src/edit.c:413
#, c-format
msgid "Are you sure you want to change user '%s' into user '%s' ? "
msgstr ""

#: ../src/edit.c:424 ../src/edit.c:426
#, c-format
msgid "Change Cancelled.\n"
msgstr ""

#: ../src/edit.c:431 ../src/edit.c:985
#, c-format
msgid "Contact address: %s\n"
msgstr ""

#: ../src/edit.c:432 ../src/edit.c:986
#, c-format
msgid "New address (%d chars): "
msgstr ""

#: ../src/edit.c:438 ../src/edit.c:991
#, c-format
msgid "New address set.\n"
msgstr ""

#: ../src/edit.c:446
#, c-format
msgid "Current Status: %s\n"
msgstr ""

#: ../src/edit.c:447
#, c-format
msgid "New Status (%d chars): "
msgstr ""

#: ../src/edit.c:454 ../src/edit.c:461
#, c-format
msgid "New status set.\n"
msgstr ""

#: ../src/edit.c:471
#, c-format
msgid "Timeout: %ld sec.\n"
msgstr ""

#: ../src/edit.c:472
#, c-format
msgid "New timeout value: "
msgstr ""

#: ../src/edit.c:493
#, c-format
msgid "TIMEOUT now disabled.\n"
msgstr ""

#: ../src/edit.c:499
#, c-format
msgid "TIMEOUT was already disabled.\n"
msgstr ""

#: ../src/edit.c:504
#, c-format
msgid "TIMEOUT must be be at least 10 minutes (600), or 0 to disable.\n"
msgstr ""

#: ../src/edit.c:512
#, c-format
msgid "New timeout set to %<PRId32> seconds.\n"
msgstr ""

#: ../src/edit.c:521
#, c-format
msgid "Lastread in folder? "
msgstr ""

#: ../src/edit.c:526
#, c-format
msgid "Folder not found.\n"
msgstr ""

#: ../src/edit.c:529
#, c-format
msgid "Last read message number %d\n"
msgstr ""

#: ../src/edit.c:530
#, c-format
msgid "New lastread: "
msgstr ""

#: ../src/edit.c:536
#, c-format
msgid "Lastread changed to %d\n"
msgstr ""

#: ../src/edit.c:540
#, c-format
msgid "Not changed.\n"
msgstr ""

#: ../src/edit.c:550
#, c-format
msgid ""
"Username: %s\n"
"Real Name: %s\n"
msgstr ""

#: ../src/edit.c:551
#, c-format
msgid ""
"Contact: %s\n"
"Status [%s]\tSpecials [%s]\n"
msgstr ""

#: ../src/edit.c:554 ../src/edit.c:736
#, c-format
msgid "Groups [%s]\n"
msgstr ""

#: ../src/edit.c:557
#, c-format
msgid "Talker: Modes=[%s]  Privs=[%s]  Protection=[%s]\n"
msgstr ""

#: ../src/edit.c:563
#, c-format
msgid "Timeout disabled.\n"
msgstr ""

#: ../src/edit.c:565
#, c-format
msgid "Timeout set to %s.\n"
msgstr ""

#: ../src/edit.c:567
#, c-format
msgid "Last Login: %s"
msgstr ""

#: ../src/edit.c:570
#, c-format
msgid "Status: %s (%s ago)\n"
msgstr ""

#: ../src/edit.c:576
#, c-format
msgid "Are you sure you want to clear %s's ignore list? "
msgstr ""

#: ../src/edit.c:582
#, c-format
msgid "Ignorelist Cleared.\n"
msgstr ""

#: ../src/edit.c:586
#, c-format
msgid "Clear Cancelled.\n"
msgstr ""

#: ../src/edit.c:589
#, c-format
msgid "Unknown Command\n"
msgstr ""

#: ../src/edit.c:627
#, c-format
msgid "Unknown folder name.\n"
msgstr ""

#: ../src/edit.c:634
#, c-format
msgid ""
"Folder %s\n"
"Current status:-\n"
msgstr ""

#: ../src/edit.c:635
#, c-format
msgid "User not in group [%s]\n"
msgstr ""

#: ../src/edit.c:637
#, c-format
msgid "User in group [%s]\n"
msgstr ""

#: ../src/edit.c:638
#, c-format
msgid "Change to :-\n"
msgstr ""

#: ../src/edit.c:639
#, c-format
msgid "User not in group [+-=][arwRWpm]: "
msgstr ""

#: ../src/edit.c:646 ../src/edit.c:657
#, c-format
msgid "Status changed to [%s]\n"
msgstr ""

#: ../src/edit.c:648 ../src/edit.c:659
#, c-format
msgid "WARNING: folder may get written over by the next folder created.\n"
msgstr ""

#: ../src/edit.c:650
#, c-format
msgid "User in group [+-=][arwRWpm]: "
msgstr ""

#: ../src/edit.c:666
#, c-format
msgid "Folder %s is currently in groups [%s]\n"
msgstr ""

#: ../src/edit.c:667
#, c-format
msgid "Folder groups [+-=][12345678]: "
msgstr ""

#: ../src/edit.c:679
#, c-format
msgid "Current folder name = %s\n"
msgstr ""

#: ../src/edit.c:680
#, c-format
msgid "Change to ? (%d chars): "
msgstr ""

#: ../src/edit.c:704
#, c-format
msgid "Name changed to %s\n"
msgstr ""

#: ../src/edit.c:709
#, c-format
msgid "First Message in folder is %d\n"
msgstr ""

#: ../src/edit.c:710
#, c-format
msgid "New first message: "
msgstr ""

#: ../src/edit.c:715
#, c-format
msgid "First message set to %d\n"
msgstr ""

#: ../src/edit.c:717
#, c-format
msgid "Last Message in folder is %d\n"
msgstr ""

#: ../src/edit.c:718
#, c-format
msgid "New Last message: "
msgstr ""

#: ../src/edit.c:723
#, c-format
msgid "Last message set to %d\n"
msgstr ""

#: ../src/edit.c:730
#, c-format
msgid ""
"\n"
"Folder Name: %s\n"
"Topic: %s\n"
"Message range %d to %d\n"
msgstr ""

#: ../src/edit.c:732
#, c-format
msgid "Status (out of group) [%s]"
msgstr ""

#: ../src/edit.c:734
#, c-format
msgid "    (in group) [%s]\n"
msgstr ""

#: ../src/edit.c:741
#, c-format
msgid "Current folder topic = '%s'\n"
msgstr ""

#: ../src/edit.c:742
#, c-format
msgid "New topic (%d chars): "
msgstr ""

#: ../src/edit.c:747
#, c-format
msgid "Topic changed to '%s'\n"
msgstr ""

#: ../src/edit.c:753
#, c-format
msgid "Do you really want to delete folder %s  ?(yes/no) "
msgstr ""

#: ../src/edit.c:772
#, c-format
msgid "Folder Deleted.\n"
msgstr ""

#: ../src/edit.c:776 ../src/edit.c:909 ../src/edit.c:922
#, c-format
msgid "Not Done.\n"
msgstr ""

#: ../src/edit.c:808
#, c-format
msgid "Message %d not found.\n"
msgstr ""

#: ../src/edit.c:817
#, c-format
msgid "You cannot edit other peoples messages.\n"
msgstr ""

#: ../src/edit.c:901
#, c-format
msgid "Currently to '%s'.\n"
msgstr ""

#: ../src/edit.c:902
#, c-format
msgid "to: "
msgstr ""

#: ../src/edit.c:906
#, c-format
msgid "Message now to '%s'\n"
msgstr ""

#: ../src/edit.c:914
#, c-format
msgid "Current subject is '%s'.\n"
msgstr ""

#: ../src/edit.c:919
#, c-format
msgid "Subject '%s'\n"
msgstr ""

#: ../src/edit.c:927
#, c-format
msgid "Message Marked for deletion.\n"
msgstr ""

#: ../src/edit.c:932
#, c-format
msgid "Message has been undeleted.\n"
msgstr ""

#: ../src/edit.c:938
#, c-format
msgid "Status mode is currently %s\n"
msgstr ""

#: ../src/edit.c:940
#, c-format
msgid "New status [%s]: "
msgstr ""

#: ../src/edit.c:946 ../src/user.c:269
#, c-format
msgid "Status set to [%s]\n"
msgstr ""

#: ../src/edit.c:950
#, c-format
msgid "What do you want to do ?\n"
msgstr ""

#: ../src/edit.c:971
#, c-format
msgid "Total Login Time of "
msgstr ""

#: ../src/edit.c:973
#, c-format
msgid "%d day%s, "
msgstr ""

#: ../src/edit.c:975
#, c-format
msgid "%d hour%s, "
msgstr ""

#: ../src/edit.c:977
#, c-format
msgid "%d minute%s, "
msgstr ""

#: ../src/edit.c:978
#, c-format
msgid "%d second%s.\n"
msgstr ""

#: ../src/folders.c:35
#, c-format
msgid "Sorry, no space for a new folder.\n"
msgstr ""

#: ../src/folders.c:43
#, c-format
msgid "Create new folder :\n"
msgstr ""

#: ../src/folders.c:44
#, c-format
msgid "Folder name (%d chars): "
msgstr ""

#: ../src/folders.c:47
#, c-format
msgid "Folder topic (%d chars): "
msgstr ""

#: ../src/folders.c:49
#, c-format
msgid "Folder status (not in group) (arwRWpm): "
msgstr ""

#: ../src/folders.c:52
#, c-format
msgid "Folder status (in group) (arwRWpm): "
msgstr ""

#: ../src/folders.c:55
#, c-format
msgid "Folder groups (12345678): "
msgstr ""

#: ../src/folders.c:60
#, c-format
msgid "Creating folder %s - %s\n"
msgstr ""

#: ../src/incoming.c:515
#, c-format
msgid ""
"\n"
"Your name has been changed to '%s'\n"
msgstr ""

#: ../src/incoming.c:660
msgid "*** Your ignore list has just been cleared ***"
msgstr ""

#: ../src/incoming.c:669
#, c-format
msgid ""
"\n"
"\n"
"--> You appear to have been banned. Goodbye... <--\r\n"
msgstr ""

#: ../src/incoming.c:675
#, c-format
msgid ""
"\n"
"\n"
"--> You appear to have been DELETED. Goodbye... <--\r\n"
msgstr ""

#: ../src/incoming.c:891
#, c-format
msgid ""
"\n"
"Boing, Zebedee arrived.  \"%s\033--\", said Zebedee\n"
msgstr ""

#: ../src/incoming.c:891
msgid "Time for bed"
msgstr ""

#: ../src/incoming.c:893
#, c-format
msgid "%s just sent the Zebedee of Death to you.\n"
msgstr ""

#: ../src/incoming.c:923
#, c-format
msgid "Boing, Zebedee's arrived.  \"Look up!\", says Zebedee\n"
msgstr ""

#: ../src/incoming.c:924
#, c-format
msgid "You look up; a large object is falling towards you very fast,\n"
msgstr ""

#: ../src/incoming.c:925
#, c-format
msgid "very very fast.  It looks like a Magic Roundabout!\n"
msgstr ""

#: ../src/incoming.c:926
#, c-format
msgid "\"I wouldn't stand there if I was you\", says Zebedee\n"
msgstr ""

#: ../src/incoming.c:927
#, c-format
msgid "Boing, Zebedee's left you standing all alone\n"
msgstr ""

#: ../src/incoming.c:928
#, c-format
msgid "WWWHHHEEEEEEEKKKKEEEERRRRRUUUUUNNNNNCCCCCHHHHHH\a\a\a\a\a\n"
msgstr ""

#: ../src/incoming.c:929
#, c-format
msgid "%s has just dropped the Magic Roundabout of Death on you.\n"
msgstr ""

#: ../src/incoming.c:932
#, c-format
msgid "\"%s\033--\" says Zebedee\n"
msgstr ""

#: ../src/init.c:95
#, c-format
msgid "Cannot load \"%s\": Illegal path\n"
msgstr ""

#: ../src/init.c:108
#, c-format
msgid "Error reading %s: Not a regular file\n"
msgstr ""

#: ../src/init.c:115
#, c-format
msgid "Error reading %s: %s\n"
msgstr ""

#: ../src/init.c:146 ../src/init.c:153
#, c-format
msgid "Malformed alias in %s at line %d\n"
msgstr ""

#: ../src/init.c:161
#, c-format
msgid "Alias %s already exists. Redefined at line %d in %s.\n"
msgstr ""

#: ../src/init.c:169 ../src/init.c:176
#, c-format
msgid "Malformed bind in %s at line %d\n"
msgstr ""

#: ../src/init.c:184
#, c-format
msgid "Bind %s already exists. Redefined at line %d in %s.\n"
msgstr ""

#: ../src/init.c:192 ../src/init.c:199
#, c-format
msgid "Malformed rpc bind in %s at line %d\n"
msgstr ""

#: ../src/init.c:207
#, c-format
msgid "RPC Bind %s already exists. Redefined at line %d in %s.\n"
msgstr ""

#: ../src/init.c:215 ../src/init.c:234
#, c-format
msgid "Malformed include in %s at line %d\n"
msgstr ""

#: ../src/init.c:246
#, c-format
msgid "Malformed event bind in %s at line %d\n"
msgstr ""

#: ../src/init.c:254
#, c-format
msgid "Event bind already exists. Useless instruction at line %d in %s.\n"
msgstr ""

#: ../src/init.c:262
#, c-format
msgid "Malformed ipc in %s at line %d\n"
msgstr ""

#: ../src/init.c:270
#, c-format
msgid "IPC bind already exists. Useless instruction at line %d in %s.\n"
msgstr ""

#: ../src/init.c:278
#, c-format
msgid "Malformed checkonoff bind in %s at line %d\n"
msgstr ""

#: ../src/init.c:286
#, c-format
msgid "Checkonoff bind already exists. Useless instruction at line %d in %s.\n"
msgstr ""

#: ../src/init.c:294
#, c-format
msgid "Malformed shutdown bind in %s at line %d\n"
msgstr ""

#: ../src/init.c:302
#, c-format
msgid "Shutdown bind already exists. Useless instruction at line %d in %s.\n"
msgstr ""

#: ../src/init.c:310
#, c-format
msgid "Malformed force bind in %s at line %d\n"
msgstr ""

#: ../src/init.c:318
#, c-format
msgid "Force bind already exists. Useless instruction at line %d in %s.\n"
msgstr ""

#: ../src/init.c:326 ../src/init.c:339
#, c-format
msgid "Malformed Script Function declaration in %s at line %d\n"
msgstr ""

#: ../src/init.c:352 ../src/init.c:365
#, c-format
msgid "Malformed Script Init declaration in %s at line %d\n"
msgstr ""

#: ../src/init.c:378 ../src/init.c:391
#, c-format
msgid "Malformed Script BoardInit declaration in %s at line %d\n"
msgstr ""

#: ../src/init.c:399
#, c-format
msgid "Loading file %s unrecognised command '%s' on line %d\n"
msgstr ""

#: ../src/init.c:425
#, c-format
msgid "Failed to get user data\n"
msgstr ""

#: ../src/init.c:430
#, c-format
msgid "Failed to read init file %s/%s\n"
msgstr ""

#: ../src/init.c:437
#, c-format
msgid "Could not find init file %s\n"
msgstr ""

#: ../src/main.c:238
msgid "\03305*** You have new mail.\n"
msgstr ""

#: ../src/main.c:244
#, c-format
msgid "\03305*** You have %d new mail messages.\n"
msgstr ""

#: ../src/main.c:470
#, c-format
msgid ""
"\n"
"Milliways accepts the optional arguments of:\n"
"\n"
msgstr ""

#: ../src/main.c:471
#, c-format
msgid "  -ae_arg <a> Pass the string 'a' in to all 'initfunc' functions as $*\n"
msgstr ""

#: ../src/main.c:472
#, c-format
msgid "  -autochat   Automatically use SUCS username to log onto talker\n"
msgstr ""

#: ../src/main.c:473
#, c-format
msgid "  -autowho    Show a 'who' list automatically on log on to talker\n"
msgstr ""

#: ../src/main.c:474
#, c-format
msgid "  -i          Internet mode (no readline)\n"
msgstr ""

#: ../src/main.c:475
#, c-format
msgid "  -new        Summary of new messages and quit\n"
msgstr ""

#: ../src/main.c:476
#, c-format
msgid "  -server <a> Connect to server <a>\n"
msgstr ""

#: ../src/main.c:477
#, c-format
msgid "  -since      Lists people logged on between now and when you last logged on\n"
msgstr ""

#: ../src/main.c:478
#, c-format
msgid "  -who        Show a list of users logged on and quit\n"
msgstr ""

#: ../src/main.c:479
#, c-format
msgid "  -what       Show a list of what users are doing\n"
msgstr ""

#: ../src/main.c:486
#, c-format
msgid ""
"\n"
"To view this help message, use the arguments:  -h, -help, or -?\n"
msgstr ""

#: ../src/main.c:487
#, c-format
msgid ""
"To specify arguments, you may use '--', or '/' instead of '-'.\n"
"\n"
msgstr ""

#: ../src/main.c:503 ../src/main.c:515 ../src/main.c:527 ../src/main.c:542
#, c-format
msgid "Username not permitted.\n"
msgstr ""

#: ../src/main.c:597 ../src/main.c:623
#, c-format
msgid "%s: User %s not found.\n"
msgstr ""

#: ../src/main.c:616
#, c-format
msgid "%s: Folder %s not found.\n"
msgstr ""

#: ../src/main.c:658
#, c-format
msgid "The board is currently locked to normal users.\n"
msgstr ""

#: ../src/main.c:661
#, c-format
msgid "The Board has been temporarily closed.\n"
msgstr ""

#: ../src/main.c:662
#, c-format
msgid "Please call again soon.\n"
msgstr ""

#: ../src/main.c:679
#, c-format
msgid "\03301%s has just entered the board."
msgstr ""

#: ../src/main.c:703
#, c-format
msgid ""
"\n"
"Last logged out %s\n"
"\n"
msgstr ""

#: ../src/main.c:705
#, c-format
msgid "Type 'talker' to enter chat mode.\n"
msgstr ""

#: ../src/main.c:710
#, c-format
msgid "You must register before being able to use this system fully.\n"
msgstr ""

#: ../src/main.c:711
#, c-format
msgid "Until then you will not be able to write in most folders or use chat mode.\n"
msgstr ""

#: ../src/main.c:712
#, c-format
msgid "Wait here for a few minutes and an administrator might register you.\n"
msgstr ""

#: ../src/main.c:714
#, c-format
msgid "Type 'help' for help.\n"
msgstr ""

#: ../src/main.c:1060
#, c-format
msgid ""
"\n"
"Error reading incoming message pipe. panic.\n"
msgstr ""

#: ../src/main.c:1064
#, c-format
msgid ""
"\n"
"Error on input terminal, argh.\n"
msgstr ""

#: ../src/main.c:1137
#, c-format
msgid "Error: Urk, no message to print.\n"
msgstr ""

#: ../src/main.c:1353
#, c-format
msgid "*** Timed Out, Good Bye\r\n"
msgstr ""

#: ../src/main.c:1357
#, c-format
msgid "\03304%s has been timed out."
msgstr ""

#: ../src/main.c:1364
#, c-format
msgid "%c*** Wakey ! Wakey !\r\n"
msgstr ""

#: ../src/main.c:2015
#, c-format
msgid "Release version %s.%s.%s\n"
msgstr ""

#: ../src/main.c:2017
#, c-format
msgid "Release version %s.%s\n"
msgstr ""

#: ../src/main.c:2019
#, c-format
msgid "Development version %s.%s.%s\n"
msgstr ""

#: ../src/main.c:2021
#, c-format
msgid "Built by %s on %s\n"
msgstr ""

#: ../src/mesg.c:22
#, c-format
msgid "User does not exist.\n"
msgstr ""

#: ../src/mesg.c:26
#, c-format
msgid "%s is not registered.\n"
msgstr ""

#: ../src/mesg.c:32
#, c-format
msgid "%s has msg off, writing anyway.\n"
msgstr ""

#: ../src/mesg.c:34
#, c-format
msgid "%s has turned messages off.\n"
msgstr ""

#: ../src/mesg.c:43
#, c-format
msgid "User not logged on.\n"
msgstr ""

#: ../src/mesg.c:52
#, c-format
msgid "Cannot inform %s of new mail.\n"
msgstr ""

#: ../src/mod.c:81
#, c-format
msgid ""
"\n"
"Message in folder %s\n"
"From: %s\n"
"To: %s\n"
"Subject: %s\n"
"Date: %s"
msgstr ""

#: ../src/mod.c:107
#, c-format
msgid "Changing to folder %s\n"
msgstr ""

#: ../src/mod.c:129
#, c-format
msgid "only %d of %d bytes read.\n"
msgstr ""

#: ../src/mod.c:133
#, c-format
msgid "[%s] (A)pprove, (L)eave, (S)ave&delete ? (<CR> to delete)"
msgstr ""

#: ../src/mod.c:135
msgid "approve"
msgstr ""

#: ../src/mod.c:144
#, c-format
msgid "Message approved as mesg %d folder %s\n"
msgstr ""

#: ../src/mod.c:146
msgid "leave"
msgstr ""

#: ../src/mod.c:149
#, c-format
msgid "Skipping message for now.\n"
msgstr ""

#: ../src/mod.c:151
msgid "save"
msgstr ""

#: ../src/mod.c:154
#, c-format
msgid "Saved and Deleted\n"
msgstr ""

#: ../src/mod.c:156
#, c-format
msgid "Deleted.\n"
msgstr ""

#: ../src/mod.c:159
#, c-format
msgid "End of folder %s\n"
msgstr ""

#: ../src/new.c:39 ../src/new.c:188 ../src/new.c:222
#, c-format
msgid "There are no folders to read !\n"
msgstr ""

#: ../src/new.c:59
#, c-format
msgid ")     Last read message %4d of %-4d  %4d new\n"
msgstr ""

#: ../src/new.c:64
#, c-format
msgid ") %4d Msgs  Topic: %s\n"
msgstr ""

#: ../src/new.c:73
#, c-format
msgid "---more--- Press <return> to continue.\r"
msgstr ""

#: ../src/new.c:90
#, c-format
msgid "Error: cannot find index file for folder %s\n"
msgstr ""

#: ../src/new.c:92
#, c-format
msgid "Error: cannot find text file for folder %s\n"
msgstr ""

#: ../src/new.c:116
#, c-format
msgid "Skipping deleted message.\n"
msgstr ""

#: ../src/new.c:132
#, c-format
msgid "Hit return for next message (%s - %d of %d)>"
msgstr ""

#: ../src/new.c:138
#, c-format
msgid "Catching up on folder %s\n"
msgstr ""

#: ../src/new.c:143
#, c-format
msgid "Skipping this folder. %d messages left unread.\n"
msgstr ""

#: ../src/new.c:157 ../src/newmain.c:891
#, c-format
msgid "Unsubscribing from %s.\n"
msgstr ""

#: ../src/new.c:203
#, c-format
msgid "Scanning folder %-*s\r"
msgstr ""

#: ../src/new.c:227
#, c-format
msgid "    Folder  Mesg        Date                 From    Subject\n"
msgstr ""

#: ../src/new.c:228
#, c-format
msgid "    ======  ====        ====                 ====    =======\n"
msgstr ""

#: ../src/new.c:240
#, c-format
msgid "            <empty>\n"
msgstr ""

#: ../src/newmain.c:58 ../src/newmain.c:97 ../src/newmain.c:121
#, c-format
msgid "Sorry, no help available on that subject.\n"
msgstr ""

#: ../src/newmain.c:83
#, c-format
msgid "No general help available for talker commands.\n"
msgstr ""

#: ../src/newmain.c:137 ../src/newmain.c:143
#, c-format
msgid "Unknown foldername.\n"
msgstr ""

#: ../src/newmain.c:148
#, c-format
msgid "Changing to folder %s.\n"
msgstr ""

#: ../src/newmain.c:182 ../src/newmain.c:556
#, c-format
msgid "No current folder.\n"
msgstr ""

#: ../src/newmain.c:186
#, c-format
msgid "Moved to end of folder. (message %d)\n"
msgstr ""

#: ../src/newmain.c:194
#, c-format
msgid "Moved to start of folder. (message %d)\n"
msgstr ""

#: ../src/newmain.c:224
#, c-format
msgid "Messages are now off.\n"
msgstr ""

#: ../src/newmain.c:226
#, c-format
msgid "Messages are already off.\n"
msgstr ""

#: ../src/newmain.c:233
#, c-format
msgid "Messages are now on.\n"
msgstr ""

#: ../src/newmain.c:235
#, c-format
msgid "Messages already on.\n"
msgstr ""

#: ../src/newmain.c:249
#, c-format
msgid "You will NOT be informed of logins/outs.\n"
msgstr ""

#: ../src/newmain.c:251
#, c-format
msgid "You are already not informed of logins.\n"
msgstr ""

#: ../src/newmain.c:258
#, c-format
msgid "You now WILL be informed of logins/outs\n"
msgstr ""

#: ../src/newmain.c:260
#, c-format
msgid "You are already informed of logins/outs.\n"
msgstr ""

#: ../src/newmain.c:273
#, c-format
msgid "Colours disabled.\n"
msgstr ""

#: ../src/newmain.c:275
#, c-format
msgid "Colour enabled.\n"
msgstr ""

#: ../src/newmain.c:277
#, c-format
msgid "No colour scheme loaded.\n"
msgstr ""

#: ../src/newmain.c:278
#, c-format
msgid "Current colour scheme: %d: %s\n"
msgstr ""

#: ../src/newmain.c:291
#, c-format
msgid "Error opening colour scheme list - %s\n"
msgstr ""

#: ../src/newmain.c:295
#, c-format
msgid "Listing available colour schemes:-\n"
msgstr ""

#: ../src/newmain.c:323
#, c-format
msgid "No colour schemes found.\n"
msgstr ""

#: ../src/newmain.c:337
#, c-format
msgid "ERROR: Cannot open colour scheme '%d'\n"
msgstr ""

#: ../src/newmain.c:352
#, c-format
msgid "Colour mode now disabled.\n"
msgstr ""

#: ../src/newmain.c:354
#, c-format
msgid "Colour mode already disabled.\n"
msgstr ""

#: ../src/newmain.c:361
#, c-format
msgid "Colour mode now enabled.\n"
msgstr ""

#: ../src/newmain.c:363
#, c-format
msgid "Colour mode already enabled.\n"
msgstr ""

#: ../src/newmain.c:377
#, c-format
msgid "You will NOT hear any beeps.\n"
msgstr ""

#: ../src/newmain.c:379
#, c-format
msgid "You have already turned beeps off.\n"
msgstr ""

#: ../src/newmain.c:386
#, c-format
msgid "You now WILL get beeps.\n"
msgstr ""

#: ../src/newmain.c:388
#, c-format
msgid "You are already getting beeps.\n"
msgstr ""

#: ../src/newmain.c:402
#, c-format
msgid "You will no longer receive wizchat.\n"
msgstr ""

#: ../src/newmain.c:404
#, c-format
msgid "You have already turned wizchat off.\n"
msgstr ""

#: ../src/newmain.c:411
#, c-format
msgid "You will now receive wizchat messages.\n"
msgstr ""

#: ../src/newmain.c:413
#, c-format
msgid "You already receive wizchat messages.\n"
msgstr ""

#: ../src/newmain.c:429
#, c-format
msgid "Forcing Subscription to folder %s "
msgstr ""

#: ../src/newmain.c:429
msgid "Off"
msgstr ""

#: ../src/newmain.c:429
msgid "On"
msgstr ""

#: ../src/newmain.c:432
#, c-format
msgid ""
"Failed\n"
"Unknown folder name %s\n"
msgstr ""

#: ../src/newmain.c:436
#, c-format
msgid ", Done.\n"
msgstr ""

#: ../src/newmain.c:450
#, c-format
msgid "Wiz! Bang! - You're a wizard again.\n"
msgstr ""

#: ../src/newmain.c:452
#, c-format
msgid "You are already a wizard.\n"
msgstr ""

#: ../src/newmain.c:460
#, c-format
msgid "!gnaB !ziW - You feel rather normal.\n"
msgstr ""

#: ../src/newmain.c:462
#, c-format
msgid "You are already rather normal.\n"
msgstr ""

#: ../src/newmain.c:491
#, c-format
msgid "You have been sent to 'coventry' you are not allowed to talk to anyone..\n"
msgstr ""

#: ../src/newmain.c:537
#, c-format
msgid "Sorry, message is %d character%s too long (would be truncated to: '%s'). Try again.\n"
msgstr ""

#: ../src/newmain.c:549
#, c-format
msgid "You are marked as doing nothing.\n"
msgstr ""

#: ../src/newmain.c:559
#, c-format
msgid "Current folder = %s\n"
msgstr ""

#: ../src/newmain.c:561
#, c-format
msgid "You haven't read any messages in this folder yet.\n"
msgstr ""

#: ../src/newmain.c:563
#, c-format
msgid "You last read message %d.\n"
msgstr ""

#: ../src/newmain.c:575
#, c-format
msgid "You are not permitted to read this folder.\n"
msgstr ""

#: ../src/newmain.c:667
#, c-format
msgid "WARNING: This command should not be run whilst people are using\n"
msgstr ""

#: ../src/newmain.c:668
#, c-format
msgid "         the bulletin board, please exercise caution.\n"
msgstr ""

#: ../src/newmain.c:804
#, c-format
msgid "Could not lockboard.\n"
msgstr ""

#: ../src/newmain.c:808
#, c-format
msgid "Board now locked.\n"
msgstr ""

#: ../src/newmain.c:813
#, c-format
msgid "Already Locked.\n"
msgstr ""

#: ../src/newmain.c:820
#, c-format
msgid "Board now unlocked.\n"
msgstr ""

#: ../src/newmain.c:823
#, c-format
msgid "Board not locked.\n"
msgstr ""

#: ../src/newmain.c:825
#, c-format
msgid "Do you want to lock or unlock it.\n"
msgstr ""

#: ../src/newmain.c:854
msgid "Enter old password: "
msgstr ""

#: ../src/newmain.c:855
#, c-format
msgid "Incorrect.\n"
msgstr ""

#: ../src/newmain.c:859
msgid "New password: "
msgstr ""

#: ../src/newmain.c:862
#, c-format
msgid "Did not match.\n"
msgstr ""

#: ../src/newmain.c:867
#, c-format
msgid "Password set.\n"
msgstr ""

#: ../src/newmain.c:876
#, c-format
msgid "You are already subscribed to %s.\n"
msgstr ""

#: ../src/newmain.c:880
#, c-format
msgid "Resubscribing to %s.\n"
msgstr ""

#: ../src/newmain.c:887
#, c-format
msgid "Already Unsubscribed from %s.\n"
msgstr ""

#: ../src/newmain.c:904
#, c-format
msgid "You are already at the beginning of the folder.\n"
msgstr ""

#: ../src/newmain.c:910
#, c-format
msgid "You are already at the end of this folder.\n"
msgstr ""

#: ../src/newmain.c:922
#, c-format
msgid "Current Status of %s\n"
msgstr ""

#: ../src/newmain.c:923
#, c-format
msgid "Your real name is %s\n"
msgstr ""

#: ../src/newmain.c:924
#, c-format
msgid "Your contact address is %s\n"
msgstr ""

#: ../src/newmain.c:925
#, c-format
msgid "Your current status is [%s]\n"
msgstr ""

#: ../src/newmain.c:926
#, c-format
msgid "Special settings are [%s]\n"
msgstr ""

#: ../src/newmain.c:937
#, c-format
msgid "Talker modes=[%s] privs=[%s] protection=[%s]\n"
msgstr ""

#: ../src/newmain.c:943
#, c-format
msgid "Talker modes=[%s] privs=[%s]\n"
msgstr ""

#: ../src/newmain.c:945
#, c-format
msgid "You have set messages %s.\n"
msgstr ""

#: ../src/newmain.c:945
msgid "off"
msgstr ""

#: ../src/newmain.c:945
msgid "on"
msgstr ""

#: ../src/newmain.c:946
#, c-format
msgid "You %s be informed of logins and logouts\n"
msgstr ""

#: ../src/newmain.c:946 ../src/newmain.c:947 ../src/newmain.c:957
msgid "will not"
msgstr ""

#: ../src/newmain.c:946 ../src/newmain.c:947 ../src/newmain.c:957
msgid "will"
msgstr ""

#: ../src/newmain.c:947
#, c-format
msgid "You %s hear beeps.\n"
msgstr ""

#: ../src/newmain.c:950
#, c-format
msgid "You can use wizchat"
msgstr ""

#: ../src/newmain.c:952
#, c-format
msgid ", but you will not hear any replies"
msgstr ""

#: ../src/newmain.c:956
#, c-format
msgid "You %s informed of user status changes.\n"
msgstr ""

#: ../src/newmain.c:958
#, c-format
msgid "You belong to the following group(s) [%s]\n"
msgstr ""

#: ../src/newmain.c:959
#, c-format
msgid "You are currently in folder %s, which you "
msgstr ""

#: ../src/newmain.c:960
#, c-format
msgid "are"
msgstr ""

#: ../src/newmain.c:960
#, c-format
msgid "are not"
msgstr ""

#: ../src/newmain.c:961
#, c-format
msgid " subscribed to.\n"
msgstr ""

#: ../src/newmain.c:963
#, c-format
msgid "You will not be timed out for being idle.\n"
msgstr ""

#: ../src/newmain.c:965
#, c-format
msgid "You will be timed out after being idle for %s.\n"
msgstr ""

#: ../src/newmain.c:1002
#, c-format
msgid "You have to read a message before you can reply to it.\n"
msgstr ""

#: ../src/newmain.c:1005
#, c-format
msgid "Replying to message %d.\n"
msgstr ""

#: ../src/newmain.c:1019
#, c-format
msgid "No folders found.\n"
msgstr ""

#: ../src/newmain.c:1024
#, c-format
msgid "Marking ALL folders as read.\n"
msgstr ""

#: ../src/newmain.c:1044
#, c-format
msgid "There is no folder '%s'\n"
msgstr ""

#: ../src/newmain.c:1050
#, c-format
msgid "There is no folder %s\n"
msgstr ""

#: ../src/newmain.c:1061
#, c-format
msgid "Marking folder %s as read.\n"
msgstr ""

#: ../src/newmain.c:1069
#, c-format
msgid "Current time and date is %s"
msgstr ""

#: ../src/newmain.c:1137 ../src/newmain.c:1161
#, c-format
msgid "TIMEOUT must be at least 10 minutes.\n"
msgstr ""

#: ../src/newmain.c:1153
#, c-format
msgid "Invalid time unit '%c' must be one of: dhms.\n"
msgstr ""

#: ../src/newmain.c:1167
#, c-format
msgid "TIMEOUT now set to %s\n"
msgstr ""

#: ../src/newmain.c:1169
#, c-format
msgid "TIMEOUT was already set to %s.\n"
msgstr ""

#: ../src/newmain.c:1184
#, c-format
msgid "Timestamps now disabled.\n"
msgstr ""

#: ../src/newmain.c:1186
#, c-format
msgid "Timestamping was already off.\n"
msgstr ""

#: ../src/newmain.c:1193
#, c-format
msgid "Timestamps now enabled.\n"
msgstr ""

#: ../src/newmain.c:1195
#, c-format
msgid "Timestamping already enabled.\n"
msgstr ""

#: ../src/newmain.c:1209
#, c-format
msgid "Posting information now suppressed.\n"
msgstr ""

#: ../src/newmain.c:1211
#, c-format
msgid "Posting info was already off.\n"
msgstr ""

#: ../src/newmain.c:1218
#, c-format
msgid "You will now be informed of new postings.\n"
msgstr ""

#: ../src/newmain.c:1220
#, c-format
msgid "Posting info was already on.\n"
msgstr ""

#: ../src/newmain.c:1234
#, c-format
msgid "User change information now suppressed.\n"
msgstr ""

#: ../src/newmain.c:1236
#, c-format
msgid "Change info was already off.\n"
msgstr ""

#: ../src/newmain.c:1243
#, c-format
msgid "You will now be informed of user status changes.\n"
msgstr ""

#: ../src/newmain.c:1245
#, c-format
msgid "Change info was already on.\n"
msgstr ""

#: ../src/newmain.c:1256
#, c-format
msgid "Already in chat mode, silly.\n"
msgstr ""

#: ../src/newmain.c:1272
#, c-format
msgid "You are not allowed to change your contact address. Please notify a SuperUser to change this for you.\n"
msgstr ""

#: ../src/newmain.c:1279
#, c-format
msgid "Alias '%s' already exists. Has now been redefined!\n"
msgstr ""

#: ../src/newmain.c:1281
#, c-format
msgid "Alias '%s' added!\n"
msgstr ""

#: ../src/newmain.c:1289
#, c-format
msgid "All Aliases Destroyed!\n"
msgstr ""

#: ../src/newmain.c:1294
#, c-format
msgid "Alias '%s' was not found!\n"
msgstr ""

#: ../src/newmain.c:1296
#, c-format
msgid "Alias '%s' was destroyed...\n"
msgstr ""

#: ../src/talker.c:429 ../src/talker.c:431
#, c-format
msgid "Current mwrc path: %s\n"
msgstr ""

#: ../src/talker.c:429
msgid "<unset>"
msgstr ""

#: ../src/talker.c:437
#, c-format
msgid "Setting mwrc path to: %s\n"
msgstr ""

#: ../src/talker.c:549
#, c-format
msgid "User '%s' is not logged on.\n"
msgstr ""

#: ../src/user.c:81
msgid "Enter Password: "
msgstr ""

#: ../src/user.c:84
#, c-format
msgid ""
"Login Incorrect.\n"
"\n"
msgstr ""

#: ../src/user.c:92
#, c-format
msgid "Sorry, this username has been banned.\n"
msgstr ""

#: ../src/user.c:93
#, c-format
msgid "Have a nice day. *:-)\n"
msgstr ""

#: ../src/user.c:101
#, c-format
msgid "Sorry, you do not have permission to run development versions of milliways.\n"
msgstr ""

#: ../src/user.c:107
#, c-format
msgid "Hello %s.\n"
msgstr ""

#: ../src/user.c:137
#, c-format
msgid "Please enter username: "
msgstr ""

#: ../src/user.c:139
#, c-format
msgid "Please enter username [%s]: "
msgstr ""

#: ../src/user.c:153
#, c-format
msgid "Don't be shy.\n"
msgstr ""

#: ../src/user.c:172
#, c-format
msgid "Problem in get_person: my uid=%d my euid=%d"
msgstr ""

#: ../src/user.c:261
#, c-format
msgid "What status do you want to be ? [rms] "
msgstr ""

#: ../src/user.c:286
#, c-format
msgid "Did I get your name right %s ? [Y]/n: "
msgstr ""

#: ../src/user.c:299
#, c-format
msgid ""
"Sorry, you do not have permission to run development versions of milliways.\n"
"Please log in again using the public version.\n"
msgstr ""

#: ../src/user.c:306
#, c-format
msgid "We use a password on this BB.\n"
msgstr ""

#: ../src/user.c:309
#, c-format
msgid "Passwords did not match.\n"
msgstr ""

#: ../src/user.c:310
msgid "Enter password: "
msgstr ""

#: ../src/user.c:311
msgid "Re-enter password: "
msgstr ""

#: ../src/user.c:316
#, c-format
msgid ""
"\n"
"Please enter the following details so that we can register you as a\n"
msgstr ""

#: ../src/user.c:317
#, c-format
msgid "normal user of this bulletin board. without correct information you\n"
msgstr ""

#: ../src/user.c:318
#, c-format
msgid "will not be allowed to use the full facilities of this board.\n"
msgstr ""

#: ../src/user.c:319
#, c-format
msgid ""
"\n"
"DATA PROTECTION ACT:\n"
msgstr ""

#: ../src/user.c:320
#, c-format
msgid "Any data entered will be recorded in a computer database for the purpose\n"
msgstr ""

#: ../src/user.c:321
#, c-format
msgid "of the administration, operation and security of the computer society. By \n"
msgstr ""

#: ../src/user.c:322
#, c-format
msgid "entering this data you consent to the storage of this data, and become an\n"
msgstr ""

#: ../src/user.c:323
#, c-format
msgid "associate member of the society.\n"
msgstr ""

#: ../src/user.c:324
#, c-format
msgid ""
"\n"
"If you do not wish to register, do not enter a name.\n"
"\n"
msgstr ""

#: ../src/user.c:327
#, c-format
msgid "Real Name: "
msgstr ""

#: ../src/user.c:332
#, c-format
msgid "User record '%s' cancelled.\n"
msgstr ""

#: ../src/user.c:333
#, c-format
msgid "Goodbye.\n"
msgstr ""

#: ../src/user.c:339
#, c-format
msgid "Email address: "
msgstr ""

#: ../src/user.c:348
#, c-format
msgid "Creating new user %s\n"
msgstr ""

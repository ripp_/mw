SRCROOT = $(CURDIR)/..
DEPTH = ../
include ../Makefile.common

POFILES = $(wildcard *.po)
MOFILES = $(POFILES:.po=.mo)
CODE = $(wildcard ../src/*.c) $(wildcard ../src/*.h)
LCDIR = $(datadir)/locale

CPY = Justin Mitchell <arthur@sucs.org>
BUGSTO = mw-devel@lists.sucs.org

build: $(MOFILES)
mw.pot: $(CODE)
	xgettext --package-version="$(VERSION)" --msgid-bugs-address="$(BUGSTO)" \
		--copyright-holder="$(CPY)" --force-po --no-wrap -E --keyword=_ \
		-o $@.new ../src/*.c ../src/*.h
	mv $@.new $@

merge: $(POFILES)
$(POFILES): mw.pot
	msgmerge -e --no-wrap --force-po $@ $< -o $@.new
	mv $@.new $@

%.mo: %.po
	msgfmt -v $< -o $@

install: $(MOFILES)
	@for m in $(^:.mo=); do\
		mkdir -vp "$(DESTDIR)$(LCDIR)/$$m/LC_MESSAGES"; \
		install -m 644 -T $$m.mo "$(DESTDIR)$(LCDIR)/$$m/LC_MESSAGES/mw.mo"; \
	done

clean:
	rm -rf *.mo share/

.PHONY: merge install build clean

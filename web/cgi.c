#define _GNU_SOURCE
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

struct cgi_object {
	char *name;
	char *value;
	
	struct cgi_object *next;
};

static struct cgi_object *list=NULL;

/* function to remove url encoding */
int url_decode(char *dst, char *src, int len)
{
	int a=0, b=0;
	int c;

	while (b<len)
	{
		if (src[b]=='%')
		{
			if (sscanf(&src[b+1],"%2x",&c)>0)
				dst[a++]=c;
			b+=3;
		}else
			dst[a++]=src[b++];
	}
	dst[a++]=0;
	return (a);
}

/* private function to split the query string up */
/* destructive to the query string */
static int cgi_split_pairs(char *query)
{
	char *start, *mid, *end;
	struct cgi_object *new;
	int count=0;

	start=query;
	while (start!=NULL && *start!=0)
	{
		int len;
		if ((end=strchr(start, '&'))==NULL)
			end=start+strlen(start);

		if ((mid=strchr(start, '='))==NULL)
			mid=end;
	
		if (mid > end) mid=end;

		if (start == mid) 
		{
			start=end+1;
			continue;
		}

		new=(struct cgi_object *)malloc(sizeof(*new));
		len=mid-start;
		new->name=(char *)malloc(len+1);
		url_decode(new->name, start, len);
	
		if (end > mid)
		{
			mid++;
			len=end-mid;
			new->value=(char *)malloc(len+1);
			url_decode(new->value, mid, len);
			new->value[len]=0;
		}else
			new->value=NULL;
	
		new->next=list;
		list=new;
		start=end;
		if (*start=='&') start++;
#ifdef DEBUG
		printf(stderr, "%d: start=0x%x '%c', mid=0x%x '%c', end=0x%x '%c'\n", count, start, *start, mid, *mid, end, *end);
#endif
		count++;
	}
	return (count );
}	
			

char *cgi_find(char *what)
{
	struct cgi_object *ptr;

	ptr=list;
	while (ptr!=NULL)
	{
		if (!strcasecmp(what, ptr->name))
			return(ptr->value);
		ptr=ptr->next;
	}
	return(NULL);
}

char *cgi_nfind(char *what, int which)
{
	struct cgi_object *ptr;
	int i=1;

	ptr=list;
	while (ptr!=NULL)
	{
		if (!strcasecmp(what, ptr->name))
		{	
			if (i==which)
				return(ptr->value);
			i++;
		}
		ptr=ptr->next;
	}
	return(NULL);
}

/* count the number of arguments,
   or number of arguments with a specific name */
int cgi_count(char *what)
{
	struct cgi_object *ptr;
	int count=0;

	ptr=list;
	while (ptr!=NULL)
	{
		if (what==NULL || !strcmp(what, ptr->name))
			count++;
		ptr=ptr->next;
	}
	return (count);
}

/* master processing function */
int cgi_process(void)
{
	static int done=0;
	char *method, *query;
	int count=0;

	if (done) return ( cgi_count(NULL) );

	if ((method=getenv("REQUEST_METHOD"))==NULL)
		return(0);

	if (!strcasecmp(method, "GET"))
	{
		if ((query=getenv("QUERY_STRING"))==NULL)
			return(0);
		query=strdup(query);
		count=cgi_split_pairs(query);	
		free(query);
	}else
	if (!strcasecmp(method, "POST"))
	{
		int len;
		if ((query=getenv("CONTENT_LENGTH"))==NULL)
			return(0);
		if ((len=atoi(query))<=0) return(0);
		query=(char *)malloc(len+1);
		read(fileno(stdin), query, len);
		query[len+1]=0;
		count=cgi_split_pairs(query);	
		free(query);
	}else
	{
		fprintf(stderr, "Unknown REQUEST_METHOD of '%s'\n", method);
		return(0);
	}

	done=1;
	return ( count );
}

#ifdef TEST

int main()
{
	int i;
	struct cgi_object *ptr;

	i=cgi_process();
	
	printf("Content-type: text/html\n\n");
	printf("<html><body>\n");
	printf("Got %d pairs<br>\n",i);

	if (i>0)
	{
		printf("<ol>\n");
		ptr=list;
		while (ptr!=NULL)
		{
			printf("<li> %s", ptr->name);
			if (ptr->value!=NULL)
				printf(" = '%s'", ptr->value);
			printf("\n");
			ptr=ptr->next;
		}
		printf("</ol>\n");
	}
	printf("</body></html>\n");
	exit(0);
}

#endif		

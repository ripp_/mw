#define _GNU_SOURCE
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#include "bb.h"
#include "db.h"

#define MAX_COUNT 10

struct mesg {
	struct mesg *next;
	struct mesg *prev;
	struct folder *ff;
	struct Header hh;
};

static struct mesg *list_head = NULL;
static struct mesg *list_tail = NULL;
static unsigned long count = 0;

static void insert(struct mesg *mm)
{
	struct mesg *p = list_head;

	if (p == NULL) {
		list_head = list_tail = mm;
		count++;
		return;
	}

	while(p && (p->hh.date < mm->hh.date))
		p = p->next;

	if (p == NULL) {
		list_tail->next = mm;
		mm->prev = list_tail;
		list_tail = mm;
		count++;
		return;
	}

	if (p == list_head) {
		list_head->prev = mm;
		mm->next = list_head;
		list_head = mm;
		count++;
		return;
	}

	mm->prev = p->prev;
	mm->next = p;
	p->prev = mm;
	mm->prev->next = mm;

	count++;
}

static void trim_head(void)
{
	struct mesg *old = list_head;

	list_head = old->next;
	list_head->prev = NULL;
	count--;

	free(old);
}

int by_date(struct folder *ff, struct Header *hh, void *arg1, void *arg2)
{
	struct mesg *mm;

	if (count >= MAX_COUNT) {
		if (hh->date < list_head->hh.date)
			return 0;
	}

	if (!person_by_name(hh->from))
		return 0;

	if ((mm = malloc(sizeof(struct mesg))) == NULL)
		return 0;

	mm->next = NULL;
	mm->prev = NULL;
	mm->ff = ff;
	memcpy(&mm->hh, hh, sizeof(struct Header));

	insert(mm);

	if (count > MAX_COUNT)
		trim_head();

	return 0;
}

struct Header *date_order_header(struct folder **ffp, struct Header *hh)
{
	struct mesg *mm = list_tail;

	if (hh == NULL) {
		if (mm) {
			*(ffp) = mm->ff;
			return &mm->hh;
		} else
			return NULL;
	}

	while(mm && (&mm->hh != hh))
		mm = mm->prev;

	if (mm) {
		mm = mm->prev;
		if (mm) {
			(*ffp) = mm->ff;
			return &mm->hh;
		}
	}

	return NULL;
}

#ifdef TEST
int main()
{
	struct person *pp;
	struct folder *ff;
	struct mesg *mm;
	int f = 0;

	db_init(1);

	pp = person_by_name("guest");
	while((ff = folder_by_number(f++)) != NULL)
		message_iterate(ff, pp, by_date, NULL, NULL);

	mm = list_head;
	while(mm) {
		printf("%s to %s on %s\n", mm->hh.from, mm->hh.to, mm->hh.subject);
		if (mm->ff)
			printf("mm->ff is set\n");
		mm = mm->next;
	}
	printf("count=%d\n", count);

	return 0;
}
#endif

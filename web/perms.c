/*********************************************************
 *     The Milliways III System is copyright 1992        *
 *      J.S.Mitchell. (arthur@sugalaxy.swan.ac.uk)       *
 *       see licence for furthur information.            *
 *********************************************************/
#include <stdio.h>
#include <fcntl.h>
#include <time.h>
#include <string.h>

#include "bb.h"
#include "perms.h"

int allowed_r(struct folder *fol, struct person *usr)
/* permission to read from folder */
{
	if (u_god(usr->status)) return(TRUE); /* superuser */
	if ((fol->groups)&(usr->groups)) /* you are in a common group */
	{
	if (u_reg(usr->status) && f_r_reg(fol->g_status)) return(TRUE); /* registered */
	if (!u_reg(usr->status) && f_r_unreg(fol->g_status)) return(TRUE); /* unregistered */
	}else  /* not in a common group */
	{
	if (u_reg(usr->status) && f_r_reg(fol->status)) return(TRUE); /* registered */
	if (!u_reg(usr->status) && f_r_unreg(fol->status)) return(TRUE); /* unregistered */
	}
	return(FALSE);
}

int is_private(struct folder *fol, struct person *usr)
{
	if ((fol->groups)&(usr->groups))
		return(f_private(fol->g_status));
	else
		return(f_private(fol->status));
}

int is_moderated(struct folder *fol, struct person *usr)
{
	if (u_god(usr->status)) return(FALSE);
	if ((fol->groups)&(usr->groups))
		return(f_moderated(fol->g_status));
	else
		return(f_moderated(fol->status));
}

int allowed_w(struct folder *fol, struct person *usr) /* permission to write to folder */
{
	if (u_god(usr->status)) return(TRUE); /* superuser */
	if ((fol->groups)&(usr->groups)) /* you are in a common group */
	{
	if (u_reg(usr->status) && f_w_reg(fol->g_status)) return(TRUE); /* registered */
	if (!u_reg(usr->status) && f_w_unreg(fol->g_status)) return(TRUE); /* unregistered */
	}else  /* not in a common group */
	{
	if (u_reg(usr->status) && f_w_reg(fol->status)) return(TRUE); /* registered */
	if (!u_reg(usr->status) && f_w_unreg(fol->status)) return(TRUE); /* unregistered */
	}
	return(FALSE);
}

int u_ban(int user) /* is banned */
{
	if (user&(1<<3))
		return(TRUE);
	else
		return(FALSE);
}

int u_del(int user) /* marked for deletion */
{
	if (user&(1<<7))
		return(TRUE);
	else
		return(FALSE);
}

int u_god(int user) /* is a superuser */
{
	if (user&(1<<2))
		return(TRUE);
	else
		return(FALSE);
}

int u_inform(int user) /* gets informed of logins/logouts */
{
	if (user&(1<<5))
		return(TRUE);
	else
		return(FALSE);
}

int u_mesg(int user) /* messages off */
{
	if (user&(1<<4))
		return(TRUE);
	else
		return(FALSE);
}

int u_beep(int user) /* beeps off */
{
	if (user&(1<<6))
		return(TRUE);
	else
		return(FALSE);
}

int u_mod(int user) /* is a moderator */
{
	if (user&(1<<1))
		return(TRUE);
	else
		return(FALSE);}

int u_reg(int user) /* is registered */
{
	if (user&1)
		return(TRUE);
	else
		return(FALSE);
}

int f_active(int stat)
{
	if (stat&1)
		return(TRUE);
	else
		return(FALSE);
}

int f_r_unreg(int stat)
{
	if (stat&(1<<1))
		return(TRUE);
	else
		return(FALSE);
}

int f_w_unreg(int stat)
{
	if (stat&(1<<2))
		return(TRUE);
	else
		return(FALSE);
}

int f_r_reg(int stat)
{
	if (stat&(1<<3))
		return(TRUE);
	else
		return(FALSE);
}

int f_w_reg(int stat)
{
	if (stat&(1<<4))
		return(TRUE);
	else
		return(FALSE);
}

int f_private(int stat)
{
	if (stat&(1<<5))
		return(TRUE);
	else
		return(FALSE);
}

int f_moderated(int stat)
{
	if (stat&(1<<6))
		return(TRUE);
	else
		return(FALSE);
}


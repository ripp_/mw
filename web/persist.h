#ifndef PERSIST_H
#define PERSIST_H

int persist_init(char *name);
char *persist_sync(char *prefix);
int persist_add(char *key, char *data);
int persist_remove(char *key);
char *persist_find(char *key);

#endif /* PERSIST_H */


#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include "bb.h"
#include "db.h"
#include "perms.h"

extern int by_date(struct folder *ff, struct Header *hh, void *arg1, void *arg2);
extern struct Header *date_order_header(struct folder **ffp, struct Header *hh);


int main(int argc, char *argv[])
{
	char *title = "Milliways BBS";
	char *link = "http://www.sucs.swan.ac.uk/cgi-bin/mw.cgi?fol=NEW";
	char *desc = "Swansea University Computer Society Bulletin Board";
	char *img_title = "Milliways";
	char *img_url = "http://www.sucs.swan.ac.uk/images/mw_small.gif";
	char *img_link = "http://www.sucs.swan.ac.uk/cgi-bin/mw.cgi?fol=NEW";
	struct person *pp;
	struct folder *ff;
	struct Header *hh = NULL;
	int i = 1;

	db_init(1);
	pp = person_by_name("guest");

	while((ff = folder_by_number(i++)) != NULL) {
		if (!f_active(ff->status) || !allowed_r(ff, pp))
			continue;
		message_iterate(ff, pp, by_date, NULL, NULL);
	}

	printf("Content-Type: text/xml\n\n");

	printf("<?xml version=\"1.0\"?><rdf:RDF\n"
		"xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\n"
		"xmlns=\"http://my.netscape.com/rdf/simple/0.9/\">\n\n");

	printf("<channel>\n");
	printf("<title>%s</title>\n", title);
	printf("<link>%s</link>\n", link);
	printf("<description>%s</description>\n", desc);
	printf("</channel>\n\n");

	if (img_title && img_url) {
		printf("<image>\n");
		printf("<title>%s</title>\n", img_title);
		printf("<url>%s</url>\n", img_url);
		printf("<link>%s</link>\n", img_link);
		printf("</image>\n\n");
	}

	while((hh = date_order_header(&ff, hh)) != NULL) {
		printf("<item>\n");
		printf("<title>%s (from %s)</title>\n", hh->subject, hh->from);
		printf("<link>http://www.sucs.swan.ac.uk/cgi-bin/mw.cgi?fol=%s&msg=%d</link>\n", ff->name, hh->Ref);
		printf("</item>\n\n");
	}

	printf("</rdf:RDF>\n");

	return 0;
}


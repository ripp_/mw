#ifndef PERMS_H
#define PERMS_H


int allowed_r(struct folder *fol, struct person *usr);
int is_private(struct folder *fol, struct person *usr);
int is_moderated(struct folder *fol, struct person *usr);
int allowed_w(struct folder *fol, struct person *usr);
int u_ban(int user);
int u_del(int user);
int u_god(int user);
int u_inform(int user);
int u_mesg(int user);
int u_beep(int user);
int u_mod(int user);
int u_reg(int user);
int f_active(int stat);
int f_r_unreg(int stat);
int f_w_unreg(int stat);
int f_r_reg(int stat);
int f_w_reg(int stat);
int f_private(int stat);
int f_moderated(int stat);


#endif /* PERMS_H */

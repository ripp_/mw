#ifndef USER_H
#define USER_H

#include <stdint.h>
#include <time.h>
#include "folders.h"
#include "rooms.h"

#define NAMESIZE 	16	/* username */
#define PASSWDSIZE 	20	/* password (after encryption) */
#define REALNAMESIZE	30	/* real name */
#define CONTACTSIZE 	60	/* contact address */
#define DOINGSIZE	79	/* 'doing' user record field */

struct person
{
	char name[NAMESIZE+1];
	char passwd[PASSWDSIZE+1];
	uint16_t pad0;
	int32_t lastlogout;
	int32_t folders[2]; /* which folders are subscribed to */
#define SETALLLONG 0xFFFFFFFF
	/* person.status
	bit	use
	0	registered
	1	is a moderator
	2	is a superuser
	3	is banned
	4	messages on/off(1=off)
	5	inform on/off(1=off)
	6	-
	7	marked for deletion
	*/
	unsigned char status;
	uint8_t pad1;
	uint16_t special;
	int32_t lastread[64]; /* last message read in each folder */
	char realname[REALNAMESIZE+1];
	char contact[CONTACTSIZE+1];
	int32_t timeused;
	int32_t idletime;
	char groups;
	char doing[DOINGSIZE];
	int32_t dowhen; /* unix time of last doing update */
	int32_t timeout;

	unsigned char spare;
	unsigned char colour;
	uint16_t room;
	uint32_t chatprivs;
	uint32_t chatmode;
};

#define MWUSR_REGD     0
#define MWUSR_MOD      1
#define MWUSR_SUPER    2
#define MWUSR_BANNED   3
#define MWUSR_MESG     4
#define MWUSR_INFORM   5
#define MWUSR_BEEPS    6
#define MWUSR_DELETED  7
#define MWUSR_SIZE     8 /* Dummy for limit checking */

#define u_ban(user)    ((user)->record.status & (1 << MWUSR_BANNED))
#define u_del(user)    ((user)->record.status & (1 << MWUSR_DELETED))
#define u_god(user)    ((user)->record.status & (1 << MWUSR_SUPER))
#define u_inform(user) ((user)->record.status & (1 << MWUSR_INFORM))
#define u_mesg(user)   ((user)->record.status & (1 << MWUSR_MESG))
#define u_beep(user)   ((user)->record.status & (1 << MWUSR_BEEPS))
#define u_mod(user)    ((user)->record.status & (1 << MWUSR_MOD))
#define u_reg(user)    ((user)->record.status & (1 << MWUSR_REGD))

struct user {
	int32_t posn;
	struct person record;
	struct folder folder;
	struct room room;
	time_t loggedin;
};

extern int userdb_open(int flags);
extern void userdb_write(struct user *user);
extern int fetch_user(struct user *record, int32_t userposn);
extern int fetch_first_user(int fd, struct user *user);
extern int fetch_next_user(int fd, struct user *user);
extern void update_user(struct user *user);
extern int update_user_fd(int fd, struct user *user);
extern int user_exists(const char *name, struct user *user);

#define for_each_user(userp, fd, status) \
	for((status) = fetch_first_user((fd), (userp)); \
	 (status) == 0; \
	 (status) = fetch_next_user((fd), (userp)))

#endif

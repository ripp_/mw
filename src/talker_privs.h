#ifndef TALKER_PRIVS_H
#define TALKER_PRIVS_H

/** user->chatmode flags **/
#define CM_ONCHAT	0x00000001
#define CM_UNUSED1	0x00000002
#define CM_GLOBAL	0x00000004
#define CM_PROTECTED	0x00000008
#define CM_FROZEN	0x00000010
#define CM_SPY		0x00000020
#define CM_VERBOSE	0x00000040
#define CM_UNUSED2	0x00000080
#define CM_GAG1		0x00000100
#define CM_GAG2		0x00000200
#define CM_GAG3		0x00000400
#define CM_GAG4		0x00000800
#define CM_GAG5		0x00001000
#define CM_GAG6		0x00002000
#define CM_UNUSED3	0x00004000
#define CM_UNUSED4	0x00008000
#define CM_STICKY	0x00010000
#define CM_UNUSED5	0x00100000

#define CM_PROTMASK	0x000E0000
#define CM_PROTSHIFT	17

#define CM_GAGMASK	0x00003F00
#define CM_GAGSHIFT	8
/* maximum gag flag used */
#define MAX_GAG_FLAG    6

/** user->chatprivs flags **/
#define CP_CANRAW	0x00000001
#define CP_CANGAG	0x00000002
#define CP_CANZOD	0x00000004
#define CP_CANMROD	0x00000008
#define CP_CANGLOBAL	0x00000010
#define CP_PROTECT	0x00000020
#define CP_FREEZE	0x00000040
#define CP_SUMMON	0x00000080
#define CP_SPY		0x00000100
#define CP_SCRIPT	0x00000200
#define CP_ADVSCR	0x00000800
#define CP_DEVEL	0x00001000
#define CP_PROTMASK	0x0000E000
#define CP_TOPIC	0x00010000

#define CP_PROTSHIFT	13

#define cm_set(user, flags)      ((user)->record.chatmode | (flags))
#define cm_test(user, flags)     (((user)->record.chatmode & (flags)) == (flags))
#define cm_test_any(user, flags) ((user)->record.chatmode & (flags))
#define cm_clear(user, flags)    ((user)->record.chatmode & ~(flags))

#define cp_set(user, flags)      ((user)->record.chatprivs | (flags))
#define cp_test(user, flags)     (((user)->record.chatprivs & (flags)) == (flags))
#define cp_test_any(user, flags) ((user)->record.chatprivs & (flags))
#define cp_clear(user, flags)    ((user)->record.chatprivs & ~(flags))

unsigned long cm_setbycode(unsigned long stat, const char *string);
unsigned long cp_setbycode(unsigned long stat, const char *string);

char *display_cmflags(unsigned long cm);
char *display_cpflags(unsigned long cm);

void show_chatmodes(unsigned long cm, char *tmp, int flag);
void show_chatprivs(unsigned long cp, char *tmp, int flag);
void show_protection(unsigned long cm, unsigned long cp, char *tmp, int ourapl);

#endif /* TALKER_PRIVS_H */

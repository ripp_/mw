#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <stdlib.h>

#include <mesg.h>
#include "bb.h"
#include "user.h"
#include "files.h"
#include "folders.h"

static void addmap(char *map, const char *name, char key,
		unsigned long size, void * offset, void * base)
{
	int i;
	unsigned long start = (unsigned long)offset - (unsigned long)base;
	printf("-%c %-20s %4lu bytes @%lu\n", key, name, size, start);
	for (i=start;i<start+size;i++)
		map[i]=key;
}

static void showmap(const char *map, unsigned long size)
{
	int i;
	printf("memmap: ");
	for (i=0;i<size;i++) {
		if (map[i]==0)
			printf(".");
		else
			printf("%c", map[i]);
		if (i % 64 == 63 && i<size-1) printf("\nmemmap: ");
	}
	printf("\n");
}

int main(int argc, char ** argv)
{
	struct folder ff;
	struct Header hh;
	struct person pp;
	char *map;

	printf("struct person = %lu bytes\n",sizeof(struct person));
	map = malloc(sizeof(struct person));
	memset(map, 0, sizeof(struct person));
	addmap(map, "name",  'N', sizeof(pp.name), &pp.name, &pp );
	addmap(map, "passwd",  'P', sizeof(pp.passwd), &pp.passwd, &pp );
	addmap(map, "lastlogout",  'l', sizeof(pp.lastlogout), &pp.lastlogout, &pp );
	addmap(map, "folders",  'f', sizeof(pp.folders), &pp.folders, &pp );
	addmap(map, "status",  's', sizeof(pp.status), &pp.status, &pp );
	addmap(map, "special",  'S', sizeof(pp.special), &pp.special, &pp );
	addmap(map, "lastread",  'L', sizeof(pp.lastread), &pp.lastread, &pp );
	addmap(map, "realname",  'R', sizeof(pp.realname), &pp.realname, &pp );
	addmap(map, "contact",  'C', sizeof(pp.contact), &pp.contact, &pp );
	addmap(map, "timeused",  't', sizeof(pp.timeused), &pp.timeused, &pp );
	addmap(map, "idletime",  'i', sizeof(pp.idletime), &pp.idletime, &pp );
	addmap(map, "groups",  'g', sizeof(pp.groups), &pp.groups, &pp );
	addmap(map, "doing",  'd', sizeof(pp.doing), &pp.doing, &pp );
	addmap(map, "dowhen",  'w', sizeof(pp.dowhen), &pp.dowhen, &pp );
	addmap(map, "timeout",  'T', sizeof(pp.timeout), &pp.timeout, &pp );
	addmap(map, "spare",  '_', sizeof(pp.spare), &pp.spare, &pp );
	addmap(map, "colour",  'c', sizeof(pp.colour), &pp.colour, &pp );
	addmap(map, "room",  'r', sizeof(pp.room), &pp.room, &pp );
	addmap(map, "chatprivs",  'C', sizeof(pp.chatprivs), &pp.chatprivs, &pp );
	addmap(map, "chatmode",  'm', sizeof(pp.chatmode), &pp.chatmode, &pp );
	showmap(map,sizeof(struct person));
	printf("\n");
	free(map);

	printf("struct header = %lu bytes\n",sizeof(struct Header));

	map = malloc(sizeof(struct Header));
	memset(map, 0, sizeof(struct Header));
	addmap(map, "Ref",  'R', sizeof(hh.Ref), &hh.Ref, &hh );
	addmap(map, "date", 'd', sizeof(hh.date), &hh.date, &hh );
	addmap(map, "from", 'f', sizeof(hh.from), &hh.from, &hh );
	addmap(map, "to",   't', sizeof(hh.to), &hh.to, &hh );
	addmap(map, "subject",'S', sizeof(hh.subject), &hh.subject, &hh );
	addmap(map, "datafield",'D', sizeof(hh.datafield), &hh.datafield, &hh );
	addmap(map, "size",   'z', sizeof(hh.size), &hh.size, &hh );
	addmap(map, "status", 's', sizeof(hh.status), &hh.status, &hh );
	addmap(map, "replyto", 'r', sizeof(hh.replyto), &hh.replyto, &hh );
	addmap(map, "spare", '_', sizeof(hh.spare), &hh.spare, &hh );
	showmap(map,sizeof(struct Header));
	printf("\n");
	free(map);


	printf("struct folder = %lu bytes\n",sizeof(struct folder));
	map = malloc(sizeof(struct folder));
	memset(map, 0, sizeof(struct folder));
	addmap(map, "status",  's', sizeof(ff.status), &ff.status, &ff );
	addmap(map, "name",  'n', sizeof(ff.name), &ff.name, &ff );
	addmap(map, "topic",  't', sizeof(ff.topic), &ff.topic, &ff );
	addmap(map, "first",  'f', sizeof(ff.first), &ff.first, &ff );
	addmap(map, "last",  'l', sizeof(ff.last), &ff.last, &ff );
	addmap(map, "g_status",  'g', sizeof(ff.g_status), &ff.g_status, &ff );
	addmap(map, "groups",  'r', sizeof(ff.groups), &ff.groups, &ff );
	addmap(map, "spare",  '_', sizeof(ff.spare), &ff.spare, &ff );
	showmap(map,sizeof(struct folder));
	printf("\n");
	free(map);

	return 0;
}


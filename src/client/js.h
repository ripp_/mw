#ifndef JS_H
#define JS_H

#include <stdio.h>
#include <jsapi.h>

int js_isrunning(void);
int js_exec(char *name, int argc, const char **argvc);
int load_jsfile(FILE *f, const char *filename);
int is_js(char *name);
void js_stop_execution(void);
int stop_js(void);
int setup_js(void);
size_t urldata(void *ptr, size_t size, size_t nmemb, void *stream);
size_t headlimit(void *ptr, size_t size, size_t nmemb, void *stream);

#endif /* JS_H */

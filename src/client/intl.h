#ifndef INTL_H
#define INTL_H

#include <libintl.h>
#define _(String) gettext (String)
#define N_(String) gettext_noop (String)

#endif /* INTL_H */

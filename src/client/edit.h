#ifndef EDIT_H
#define EDIT_H

#include "user.h"

void time_on(long u);
void edit_user(const char *args, const char *name);
void edit_folder(const char *args, const char *name);
void edit_contact(void);
void mesg_edit(const char *args, struct folder *folder, int msgno, struct user *usr);

#endif /* EDIT_H */
